/**
 * Checks if document is ready, and calls the callback function if it is.
 * If the document is not ready, the function calls itself until it is.
 *
 */
function checkIfDocumentReady(callback) {
    callback = (typeof callback === 'function') ? callback : function() {};

    while (document.readyState !== 'complete') {

        setTimeout(function() {
            checkIfDocumentReady(callback);
        }, 10);

        return;
    }

    callback();
}

/**
 * Initialises the program.
 */
function init() {

    // Attach handlers
    document.forms['expression-form'].onsubmit = function(e) {
        e.preventDefault();

        // Done function
        var done = function () {
            generate();
        };

        // Help DOM element
        var _help = _(document.getElementById('help'));

        if (!_help.hasClass('hide')) {
            _help.addClass('hide');
            setTimeout(done, 300);
        } else {
            done();
        }
    }

    document.getElementsByClassName('dropdown')[0].onclick = function(e) {
        _(document.getElementById('suggestions-wrapper')).toggleClass('hide');
    }

    var links = document.getElementById('suggestions-wrapper').getElementsByTagName('a');

    for (var l = 0; l < links.length; l++) {
        links[l].onclick = function(e) {
            document.getElementsByClassName('dropdown')[0].onclick(e);
            document.getElementById('expression').value = e.srcElement.getAttribute('data-exec');
            document.forms['expression-form'].onsubmit(e);
        };
    }
}

/**
 * Given a list of variable names, calculates all possible combinations of those variables as represented by boolean logic.
 * For example, an input of ["a", "b"] will produce an array with four objects:
 *
 *     [
 *         {a: false, b:false },
 *         {a: true, b:false },
 *         {a: false, b:true },
 *         {a: true, b:true }
 *     ]
 *
 *
 * @param var_names {String[]} A array of string variable names
 *
 * @return An array of objects containing all possible combinations of the supplied variables in boolean logic (see function description)
 */
function calculateCombinations(var_names) {
    var n = var_names.length;

    var combinations = [];

    if (n <= 0) {
        return;
    }

    //if n = 2, nTemp = 3
    //if n = 3, nTemp = 7
    //if n = 4, nTemp = 15
    //....

    var nTemp = Math.pow(2, n);

    for (var i = 0; i < nTemp; i++) {

        var combination = {};

        for (var k = 0; k < var_names.length; k++) {
            if ((i >> k) & 0x1) {
                combination[var_names[k]] = true;
            } else {
                combination[var_names[k]] = false;
            }
        }

        combinations.push(combination);
    }

    return combinations;
}

/**
 *
 *
 */
function getUniqueVariables(inputString) {
    return inputString.match(/\w+/g).unique();
}

/**
 * Generates the truth table.
 *
 * Takes the input from the text field, and replaces a and b with true or false, and then
 * evaluates the input to build up the truth table.
 *
 */
function generate() {

    // Get input
    var input = document.getElementById('expression');

    var stringInput = input.value;

    var id = "table-container_"+(+new Date());

    var map = [
        {
            a: true,
            b: true
        },
        {
            a: true,
            b: false
        },
        {
            a: false,
            b: true
        },
        {
            a: false,
            b: false
        }
    ];

    map = calculateCombinations(getUniqueVariables(stringInput));

    // Clear error class from input
    input.setAttribute('class', (input.getAttribute('class') || "").replace("error", ""));

    // Clone table element and show
    var newTable = document.getElementById('table-container').cloneNode(true);
    newTable.setAttribute('class', newTable.getAttribute('class').replace(/hidden/g, ""));
    newTable.setAttribute('id', id);

    // Set table title
    newTable.getElementsByTagName('span')[0].textContent = stringInput;

    // Make table header
    var theadRow = newTable.getElementsByTagName('thead')[0].getElementsByTagName('tr')[0];
    for (var h in map[0]) {

        var th = document.createElement('th');
        th.textContent = h;
        theadRow.appendChild(th);
    }

    th = document.createElement('th');
    th.textContent = stringInput;
    theadRow.appendChild(th);

    // Loop through each combination of true/false
    for (var i = 0; i < map.length; i++) {

        // String to evaluate
        var curExpression = stringInput;

        // Replace terms with values from map
        for (var m in map[i]) {
            curExpression = curExpression.replace(new RegExp(m, "g"), map[i][m]);
        }

        // Get table cells
        var tbodyRow = document.createElement('tr');

        // Add "odd" class if row is odd numbered
        tbodyRow.setAttribute('class', i % 2 !== 0 ? "odd" : "even");
        newTable.getElementsByTagName('tbody')[0].appendChild(tbodyRow);

        // Find correct cells to populate
        for (var c in map[i]) {

            var td = document.createElement('td');
            td.textContent = map[i][c] ? 'T' : 'F';
            td.setAttribute('data-value', map[i][c]);
            tbodyRow.appendChild(td);
        }

        td = document.createElement('td');

        // Try to evaluate expression
        try {
            td.textContent = eval(curExpression) ? "T" : "F";
            td.setAttribute('data-value', td.textContent === "T");
        } catch(e) {
            input.setAttribute('class', input.getAttribute('class').concat(" error"));
            return;
        }

        tbodyRow.appendChild(td);
    }

    // Attach event handlers
    attachTableHandlers(newTable);

    // Append to dom
    document.getElementById('table-list').insertBefore(newTable, document.getElementById('table-list').getElementsByClassName('generated-table')[0]);
}

/**
 * Attaches handlers, such as click listeners, to elements within the new table container.
 */
function attachTableHandlers(table) {
    table.getElementsByClassName('remove')[0].onclick = function(e) {
        table.setAttribute('class', table.getAttribute('class').concat(' hide'));
        setTimeout(function() {
            table.remove();

            if (document.getElementsByTagName('table').length === 1) {

                // Help DOM element
                var _help = _(document.getElementById('help'));
                _help.removeClass('hide');
            }
        }, 1000);
    };
}

// Init
checkIfDocumentReady(init);

/* Extend Array prototype to have a unique method */
Array.prototype.unique = function() {
    var o = {}, i, l = this.length, r = [];
    for(i=0; i<l;i+=1) o[this[i]] = this[i];
    for(i in o) r.push(o[i]);
    return r;
};


var _ = function (element) {

    this.el = element;

    this.addClass = function(klass) {
        if (!this.hasClass(klass)) {
            this.el.setAttribute('class', (this.el.getAttribute('class') || "").concat(" ").concat(klass));
        }
    };

    this.removeClass = function(klass) {
        this.el.setAttribute('class', this.el.getAttribute('class').replace(new RegExp(klass, "g"), ""));
    };

    this.hasClass = function(klass) {
        return this.el.getAttribute('class') && this.el.getAttribute('class').indexOf(klass) !== -1;
    };

    this.toggleClass = function(klass) {
        if (this.hasClass(klass)) {
            this.removeClass(klass);
        } else {
            this.addClass(klass);
        }
    };

    return this;
};